<?php

require_once __DIR__ . '/../../lib/func.php';

$HTMLout = "";
$HTMLout .= "<!doctype html>\n";
$HTMLout .= "<head><title>Tomek's Lair</title>\n";
$HTMLout .= "<style>\n";
$HTMLout .= "body { background-color: black; }\n";
$HTMLout .= "a:link {    color: white;}\n";
$HTMLout .= "a:visited {    color: white;}\n";
$HTMLout .= "a:hover {    color: white;}\n";
$HTMLout .= "a:active {    color: white;}\n";
$HTMLout .= ".white {    color: white;}\n";

$HTMLout .= "</style></head>\n";

$HTMLout .= "<body>";

if (isset($_POST['submit'])) {
    require_once __DIR__ . '/../../lib/Torrent.php';
    $torrent = new Torrent($_FILES["fileToUpload"]["tmp_name"]);

    $files = $torrent->content();
    foreach ($files as $k => $v) {
        $firstFile = $k;
        break;
    }
    if (strpos($firstFile, "/") === false) {
        echo "Single-file torrents not allowed";
    } else {
        $directory = substr($firstFile, 0, strpos($firstFile, "/"));
        $torrent->announce(false);
        $torrent->announce('http://announce.tomekslair.org/announce.php?passkey=dupa.8'); // add a tracker
        $torrent->name($directory);
        $torrent->comment("");
        $torrent->is_private(true);

        $target_dir = "../../uploads/";
        $target_file = $target_dir . $directory . ".torrent";

        echo "The file " . $directory . " has been uploaded.";

        $torrent->save($target_file);
    }

}



$HTMLout .= menu("upload", null, null);

$HTMLout .= "<form action=\"\" method=\"post\" enctype=\"multipart/form-data\">";
$HTMLout .= "<span class='white'>Select torrent to upload:</span><br>";
$HTMLout .= "<input type=\"file\" name=\"fileToUpload\" id=\"fileToUpload\"><br>";
$HTMLout .= "<input type=\"submit\" value=\"Upload\" name=\"submit\"><br>";
$HTMLout .= "</form>";

$HTMLout .= "</body>";
$HTMLout .= "</html>";

echo $HTMLout;

?>