<?php

require_once __DIR__ . '/../../../lib/properties.php';

$LETTER = "_LETTER_";

$HTMLHead = "";
$HTMLHead .= "<!doctype html>\n";
$HTMLHead .= "<head><title>Tomek's Lair</title>\n";
$HTMLHead .= "<style>\n";
$HTMLHead .= "body { background-color: black; }\n";
$HTMLHead .= "a:link {    color: white;}\n";
$HTMLHead .= "a:visited {    color: white;}\n";
$HTMLHead .= "a:hover {    color: white;}\n";
$HTMLHead .= "a:active {    color: white;}\n";
$HTMLHead .= ".white {    color: white;}\n";
$HTMLHead .= "</style></head>\n";

$HTMLBody = "";
$HTMLBody .= "<body>";

require_once __DIR__ . '/../../../lib/func.php';

$HTMLFiles = "";
$files = scandir("../../../uploads");
foreach ($files as $value) {
    if ($value == "." || $value == "..") {
        continue;
    }
    if (substr($value, 0, 1) == $LETTER) {
        $HTMLFiles .= "<a href=\"?file=" . $value . "\">" . $value . "</a><br>";
    }
}

$HTMLout = "";
$HTMLout .= $HTMLHead;
$HTMLout .= "<body>";

$HTMLMenu = menu("browse", $LETTER, null);

$HTMLout .= $HTMLMenu;
if ($HTMLMenu != "" && $HTMLFiles != "") {
    $HTMLout .= "<br><span class='white'>---</span><br>";
}
$HTMLout .= $HTMLFiles;

$HTMLout .= "</body>";
echo $HTMLout;

?>