<?php

require_once __DIR__ . '/../../../../lib/properties.php';
require_once __DIR__ . '/../../../../lib/func.php';

$LETTER = "_LETTER_";
$LINK = "_LINK_";

$HTMLHead = "";
$HTMLHead .= "<!doctype html>\n";
$HTMLHead .= "<head><title>Tomek's Lair</title>\n";
$HTMLHead .= "<style>\n";
$HTMLHead .= "body { background-color: black; }\n";
$HTMLHead .= "a:link {    color: white;}\n";
$HTMLHead .= "a:visited {    color: white;}\n";
$HTMLHead .= "a:hover {    color: white;}\n";
$HTMLHead .= "a:active {    color: white;}\n";
$HTMLHead .= ".white {    color: white;}\n";
$HTMLHead .= "</style></head>\n";

$HTMLBody = "";
$HTMLBody .= "<body>";

$HTMLMenu = menu("browse", $LETTER, $LINK);

$link = mysqli_connect($MYSQL_HOST, $MYSQL_USER, $MYSQL_PASSWORD, $MYSQL_DATABASE);
if (!$link) {
    die(__DIR__ . "/index.php:28");
}

$sql = "select filterSQL from tvshows WHERE link = '" . $LINK . "'";
$result = mysqli_query($link, $sql);
if (!$result) {
    die(__DIR__ . "/index.php:34");
}

if ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $filterSQL = $row['filterSQL'];
}

if (!isset($filterSQL)) {
    die(__DIR__ . "/index.php:42");
}
$HTMLFiles = "";

$sql = "SELECT * FROM releases WHERE " . $filterSQL . " AND EpisodeFlags = 'ENF1FD0DD0L0U0' ORDER BY OrderField";
$result = mysqli_query($link, $sql);
if (!$result) {
    error_log($sql);
    error_log(mysqli_error($link));
    die("ERROR (3)");
}

$torrents = array();

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    if ($row["Release720pHDTVx264"] != "") {
        if ($row["Release720pHDTVx264Flags"][5] != 'F') {
            $file = "../../../../uploads/" . $row["Release720pHDTVx264"] . ".torrent";
            if (file_exists($file)) {
                $HTMLFiles .= "<a href=\"../../../download/?file=" . $row["Release720pHDTVx264"] . ".torrent\">" . $row["Release720pHDTVx264"] .
                    "</a><br>";
                $torrents[$row["Release720pHDTVx264"].".torrent"] = 1;
            } else {
                $HTMLFiles .= "<span class='white'>" . $row["Release720pHDTVx264"] . "</span><br>";
            }
        }
    }
    if ($row["Release720pHDTVx2642"] != "") {
        if ($row["Release720pHDTVx2642Flags"][5] != 'F') {
            $file = "../../../../uploads/" . $row["Release720pHDTVx2642"] . ".torrent";
            if (file_exists($file)) {
                $HTMLFiles .= "<a href=\"../../../download/?file=" . $row["Release720pHDTVx2642"] . ".torrent\">" . $row["Release720pHDTVx2642"] .
                    "(" . $row["Release720pHDTVx2642Flags"] . ")" .
                    "</a><br>";
                $torrents[$row["Release720pHDTVx2642"].".torrent"] = 1;
            } else {
                error_log($file);
                $HTMLFiles .= "<span class='white'>" . $row["Release720pHDTVx2642"] . "</span><br>";
            }
        }
    }
}


$files = scandir("../../../../uploads");
foreach ($files as $value) {
    if ($value == "." || $value == "..") {
        continue;
    }
    if (substr($value, 0, 1) == $LETTER) {
        if(!isset($torrents[$value])) {
            $HTMLFiles .= "<a href=\"../../../download/?file=" . $value . "\">" . $value . "</a><br>";
        }
    }
}

$HTMLout = "";
$HTMLout .= $HTMLHead;
$HTMLout .= "<body>";

$HTMLout .= $HTMLMenu;
if ($HTMLMenu != "" && $HTMLFiles != "") {
    $HTMLout .= "<br><span class='white'>---</span><br>";
}
$HTMLout .= $HTMLFiles;

$HTMLout .= "</body>";
echo $HTMLHead . $HTMLout;

?>