<?php

if (!isset($_GET["file"])) {
    die(__DIR__ . "/index.php:4");
}

$file = "../../uploads/" . $_GET["file"];

if (!file_exists($file)) {
    die(__DIR__ . "/index.php:10");
}

header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=' . basename($file));
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . filesize($file)); //Remove
readfile($file);

?>