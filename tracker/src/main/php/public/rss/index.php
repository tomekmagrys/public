<?php

header("Content-Type: application/xml; charset=ISO-8859-1");

require_once __DIR__ . '/../../lib/Torrent.php';

$HTMLout = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

$HTMLout .= "<rss version=\"2.0\" xmlns:torrent=\"http://xmlns.ezrss.it/0.1/\">\n";
$HTMLout .= "    <channel>\n";
$HTMLout .= "        <title>Tomek's Lair RSS feed</title>\n";
$HTMLout .= "        <link>https://eztv.ag/</link>\n";
$HTMLout .= "        <description>Tomek's Lair RSS feed</description>\n";

$files = scandir("../../uploads/");


foreach ($files as $value) {
    if ($value == "." || $value == "..") {
        continue;
    }

    $torrent = new Torrent("../../uploads/" . $value);

    $HTMLout .= "        <item>\n";
    $HTMLout .= "            <title>" . substr($value, 0, -8) . "</title>\n";
    $HTMLout .= "            <category>TEST</category>\n";
    $HTMLout .= "            <author>https://tracker.tomekslair.org/</author>\n";
    $HTMLout .= "            <link>https://tracker.tomekslair.org/browse/?file=" . $value . "</link>\n";
    $HTMLout .= "            <guid>https://tracker.tomekslair.org/browse/?file=" . $value . "</guid>\n";
    $HTMLout .= "            <pubDate>Fri, 26 Feb 2016 21:05:47 -0600</pubDate>\n";
    $HTMLout .= "            <torrent:contentLength>" . $torrent->size() . "</torrent:contentLength>\n";
    $HTMLout .= "            <torrent:infoHash>" . $torrent->hash_info() . "</torrent:infoHash>\n";
    $HTMLout .= "            <torrent:magnetURI><![CDATA[" . $torrent->magnet(false) . "]]></torrent:magnetURI>\n";
    $HTMLout .= "            <torrent:seeds>0</torrent:seeds>\n";
    $HTMLout .= "            <torrent:peers>0</torrent:peers>\n";
    $HTMLout .= "            <torrent:verified>0</torrent:verified>\n";
    $HTMLout .= "            <torrent:fileName>" . substr($value, 0, -8) . "</torrent:fileName>\n";
    $HTMLout .= "            <enclosure url=\"https://".$_SERVER['PHP_AUTH_USER'].":".$_SERVER['PHP_AUTH_PW']."@tracker.tomekslair.org/browse/?file=" . $value . "\" length=\"" . $torrent->size() . "\" type=\"application/x-bittorrent\"/>\n";
    $HTMLout .= "        </item>\n";

}

$HTMLout .= "    </channel>\n";
$HTMLout .= "</rss>";

echo $HTMLout;

?>