<?php

/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <phk@FreeBSD.ORG> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */

function bencode_error($error)
{
    return "d" .
    strlen("failure reason") . ":" . "failure reason" .
    strlen($error) . ":" . $error .
    "e";
}

require_once __DIR__ . '/../config/config.php';


$HTMLout = "";
$HTMLout .= "d8:intervali" . "3600" . "e";

$response["interval"] = 3600;


$link = mysqli_connect($MYSQL_HOST, $MYSQL_USER, $MYSQL_PASSWORD, $MYSQL_DATABASE);

if (!$link) {
    echo bencode_error("ERROR (1)");
    exit;
}

$sql = "DELETE FROM announce WHERE info_hash = '" .
    mysqli_real_escape_string($link, $_GET["info_hash"]) .
    "' AND ip = '" . $_SERVER['REMOTE_ADDR'] . "'";
$result = mysqli_query($link, $sql);

if (!$result) {
    error_log($sql);
    error_log(mysqli_error($link));
    echo bencode_error("ERROR (2)");
    exit;
}

$sql = "INSERT INTO announce (peer_id,info_hash,ip,port) VALUES ('" .
    mysqli_real_escape_string($link, $_GET["peer_id"]) .
    "','" .
    mysqli_real_escape_string($link, $_GET["info_hash"]) .
    "','" .
    $_SERVER['REMOTE_ADDR'] . "','" . $_GET["port"] . "')";
$result = mysqli_query($link, $sql);

if (!$result) {
    error_log($sql);
    error_log(mysqli_error($link));
    echo bencode_error("ERROR (3)");
    exit;
}


$sql = "select max(peer_id) AS peer_id, ip, max(port) as port from announce  WHERE info_hash = '" .
    mysqli_real_escape_string($link, $_GET["info_hash"]) . "' GROUP BY ip";
$result = mysqli_query($link, $sql);
if (!$result) {
    error_log($sql);
    error_log(mysqli_error($link));
    echo bencode_error("ERROR (4)");
    exit;
}

$compact = 0;

if (isset($_GET["compact"])) {
    if ($_GET["compact"] == 1) {
        $compact = 1;
        $peers = "";
    }
}

if ($compact == 0) {
    $HTMLout .= "5:peers" . "l";
}

while ($row = mysqli_fetch_assoc($result)) {
    $port = intval($row["port"]);
    if ($compact == 0) {
        $peer["peer id"] = $row["peer_id"];
        $peer["ip"] = $row["ip"];
        $peer["port"] = $port;
        $peers[] = $peer;
        $HTMLout .= "d2:ip" . strlen($peer["ip"]) . ":" . $peer["ip"] .
            "7:peer id" . strlen($peer["peer id"]) . ":" . $peer["peer id"] .
            "4:port" . "i" . $peer["port"] . "ee";
    } else {
        $peer = chr($port / 256) . chr($port % 256);
        $ip = ip2long($row["ip"]);
        for ($i = 0; $i < 4; $i++) {
            $peer = chr($ip % 256) . $peer;
            $ip = $ip / 256;
        }
        $peers .= $peer;
    }
}


mysqli_close($link);

if ($compact == 0) {
    $HTMLout .= "ee";
} else {
    $HTMLout .= "5:peers" . strlen($peers) . ":" . $peers . "e";
}

$response["peers"] = $peers;


error_log($HTMLout);
echo $HTMLout;

?>