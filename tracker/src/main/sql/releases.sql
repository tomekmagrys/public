CREATE TABLE IF NOT EXISTS releases (
  id INT AUTO_INCREMENT,
  PRIMARY KEY (id)
);


DELIMITER $$

DROP PROCEDURE IF EXISTS create_category $$
CREATE PROCEDURE create_category()
  BEGIN

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'EpisodeTitle' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD EpisodeTitle VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'EpisodeTitleFlags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD EpisodeTitleFlags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseName' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseName VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseNameFlags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseNameFlags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleasePDTVXviD' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleasePDTVXviD VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleasePDTVXviDFlags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleasePDTVXviDFlags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseHDTVXviD' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseHDTVXviD VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseHDTVXviDFlags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseHDTVXviDFlags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseHDTVXviD2' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseHDTVXviD2 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseHDTVXviD2Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseHDTVXviD2Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseHDTVXviD3' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseHDTVXviD3 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseHDTVXviD3Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseHDTVXviD3Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseHDTVXviD4' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseHDTVXviD4 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseHDTVXviD4Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseHDTVXviD4Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseHDTVX264' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseHDTVX264 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseHDTVX264Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseHDTVX264Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseReencodeHRHDTVXviD' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseReencodeHRHDTVXviD VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseReencodeHRHDTVXviDFlags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseReencodeHRHDTVXviDFlags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release480pWEBDLx264' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release480pWEBDLx264 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release480pWEBDLx264Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release480pWEBDLx264Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release576pWEBDLnSDx264' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release576pWEBDLnSDx264 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release576pWEBDLnSDx264Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release576pWEBDLnSDx264Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pHDTVx264' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pHDTVx264 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pHDTVx264Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pHDTVx264Flags VARCHAR(29) NOT NULL DEFAULT '';
    ELSE
      ALTER TABLE releases MODIFY COLUMN Release720pHDTVx264Flags VARCHAR(29) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pHDTVx2642' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pHDTVx2642 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pHDTVx2642Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pHDTVx2642Flags VARCHAR(29) NOT NULL DEFAULT '';
    ELSE
      ALTER TABLE releases MODIFY COLUMN Release720pHDTVx2642Flags VARCHAR(29) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pHDTVx2643' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pHDTVx2643 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pHDTVx2643Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pHDTVx2643Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pWEBDLDD51x264' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pWEBDLDD51x264 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pWEBDLDD51x264Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pWEBDLDD51x264Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pWEBDLDD51H2642' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pWEBDLDD51H2642 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pWEBDLDD51H2642Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pWEBDLDD51H2642Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pWEBCAPAVC' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pWEBCAPAVC VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pWEBCAPAVCFlags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pWEBCAPAVCFlags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseWEBCAPAVC' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseWEBCAPAVC VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseWEBCAPAVCFlags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseWEBCAPAVCFlags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release1080iHDTVDD51MPEG2' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release1080iHDTVDD51MPEG2 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release1080iHDTVDD51MPEG2Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release1080iHDTVDD51MPEG2Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pBluRayx264' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pBluRayx264 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pBluRayx264Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pBluRayx264Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pBluRayx2642' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pBluRayx2642 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pBluRayx2642Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pBluRayx2642Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pBluRayDD51x264' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pBluRayDD51x264 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release720pBluRayDD51x264Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release720pBluRayDD51x264Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release1080pBluRayx264' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release1080pBluRayx264 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release1080pBluRayx264Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release1080pBluRayx264Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseBDRipXviD' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseBDRipXviD VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseBDRipXviDFlags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseBDRipXviDFlags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseDVDRipXviD' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseDVDRipXviD VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseDVDRipXviDFlags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseDVDRipXviDFlags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseDVDRipXviD2' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseDVDRipXviD2 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseDVDRipXviD2Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseDVDRipXviD2Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseNTSCDVDR' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseNTSCDVDR VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleaseNTSCDVDRFlags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleaseNTSCDVDRFlags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleasePortable' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleasePortable VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ReleasePortableFlags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD ReleasePortableFlags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release1080pBluRayx2642' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release1080pBluRayx2642 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release1080pBluRayx2642Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release1080pBluRayx2642Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release1080pBluRayx2643' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release1080pBluRayx2643 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release1080pBluRayx2643Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release1080pBluRayx2643Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release1080pBluRayx2644' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release1080pBluRayx2644 VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'Release1080pBluRayx2644Flags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD Release1080pBluRayx2644Flags VARCHAR(1) NOT NULL DEFAULT '_';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'EpisodeFlags' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD EpisodeFlags VARCHAR(21) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'OrderField' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD OrderField VARCHAR(256) DEFAULT NULL;
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'FakeField' AND TABLE_NAME = 'releases'))
    THEN
      ALTER TABLE releases ADD FakeField int(11) NOT NULL DEFAULT '0';
    END IF;

  END $$

CALL create_category() $$

DELIMITER ;
