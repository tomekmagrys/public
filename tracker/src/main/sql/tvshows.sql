CREATE TABLE IF NOT EXISTS tvshows (
  id INT AUTO_INCREMENT,
  PRIMARY KEY (id)
);


DELIMITER $$

DROP PROCEDURE IF EXISTS create_category $$
CREATE PROCEDURE create_category()
  BEGIN

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'link' AND TABLE_NAME = 'tvshows'))
    THEN
      ALTER TABLE tvshows ADD link VARCHAR(20) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'title' AND TABLE_NAME = 'tvshows'))
    THEN
      ALTER TABLE tvshows ADD title VARCHAR(20) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'category' AND TABLE_NAME = 'tvshows'))
    THEN
      ALTER TABLE tvshows ADD category VARCHAR(20) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'category2' AND TABLE_NAME = 'tvshows'))
    THEN
      ALTER TABLE tvshows ADD category2 VARCHAR(20) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'filterSQL' AND TABLE_NAME = 'tvshows'))
    THEN
      ALTER TABLE tvshows ADD filterSQL VARCHAR(256) NOT NULL DEFAULT '';
    ELSE
      ALTER TABLE tvshows MODIFY filterSQL VARCHAR(256) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'myEpisodesId' AND TABLE_NAME = 'tvshows'))
    THEN
      ALTER TABLE tvshows ADD myEpisodesId VARCHAR(20) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'episodeFlags' AND TABLE_NAME = 'tvshows'))
    THEN
      ALTER TABLE tvshows ADD episodeFlags VARCHAR(20) NOT NULL DEFAULT '';
    END IF;


END $$

CALL create_category() $$

DELIMITER ;
