CREATE TABLE IF NOT EXISTS announce (
  id INT AUTO_INCREMENT,
  PRIMARY KEY (id)
);


DELIMITER $$

DROP PROCEDURE IF EXISTS create_announce $$
CREATE PROCEDURE create_announce()
  BEGIN

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'peer_id' AND TABLE_NAME = 'announce'))
    THEN
      ALTER TABLE announce ADD peer_id VARCHAR(20) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'info_hash' AND TABLE_NAME = 'announce'))
    THEN
      ALTER TABLE announce ADD info_hash VARCHAR(20) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'port' AND TABLE_NAME = 'announce'))
    THEN
      ALTER TABLE announce ADD port VARCHAR(5) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'uploaded' AND TABLE_NAME = 'announce'))
    THEN
      ALTER TABLE announce ADD uploaded VARCHAR(255) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'downloaded' AND TABLE_NAME = 'announce'))
    THEN
      ALTER TABLE announce ADD downloaded VARCHAR(255) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'left' AND TABLE_NAME = 'announce'))
    THEN
      ALTER TABLE announce ADD `left` VARCHAR(255) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'corrupt' AND TABLE_NAME = 'announce'))
    THEN
      ALTER TABLE announce ADD corrupt VARCHAR(255) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'key' AND TABLE_NAME = 'announce'))
    THEN
      ALTER TABLE announce ADD `key` VARCHAR(255) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'event' AND TABLE_NAME = 'announce'))
    THEN
      ALTER TABLE announce ADD event VARCHAR(255) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'numwant' AND TABLE_NAME = 'announce'))
    THEN
      ALTER TABLE announce ADD numwant VARCHAR(3) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'compact' AND TABLE_NAME = 'announce'))
    THEN
      ALTER TABLE announce ADD compact VARCHAR(3) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'no_peer_id' AND TABLE_NAME = 'announce'))
    THEN
      ALTER TABLE announce ADD no_peer_id VARCHAR(3) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ipv6' AND TABLE_NAME = 'announce'))
    THEN
      ALTER TABLE announce ADD ipv6 VARCHAR(25) NOT NULL DEFAULT '';
    END IF;

    IF NOT EXISTS((SELECT *
                   FROM information_schema.COLUMNS
                   WHERE TABLE_SCHEMA = DATABASE()
                         AND COLUMN_NAME = 'ip' AND TABLE_NAME = 'announce'))
    THEN
      ALTER TABLE announce ADD ip VARCHAR(25) NOT NULL DEFAULT '';
    END IF;

END $$

CALL create_announce() $$

DELIMITER ;
